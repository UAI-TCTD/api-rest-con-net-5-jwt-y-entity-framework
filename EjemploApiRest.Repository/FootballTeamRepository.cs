﻿using EjemploApiRest.Abstractions;
using EjemploApiRest.DataAccess;
using EjemploApiRest.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploApiRest.Repository
{
    public interface IFootballTeamRepository : IRepository<FootballTeam>
    {
        FootballTeam GetFootballTeamByManager(string manager);
    }
    public class FootballTeamRepository : Repository<FootballTeam>, IFootballTeamRepository
    {
        IFootballTeamDbContext _ctx;

        public FootballTeamRepository(IFootballTeamDbContext ctx, ICacheService<FootballTeam> cache) :base(ctx,cache)
        {
            _ctx = ctx;
        }

        public FootballTeam GetFootballTeamByManager(string manager)
        {
            return _ctx.GetFootballTeamByManager(manager);
        }
    }
}
