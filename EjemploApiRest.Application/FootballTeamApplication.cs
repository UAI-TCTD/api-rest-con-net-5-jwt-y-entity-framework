﻿using EjemploApiRest.Entities;
using EjemploApiRest.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploApiRest.Application
{
    public interface IFootballTeamApplication : IApplication<FootballTeam>
    {
        FootballTeam GetFootballTeamByManager(string manager);
    }
    public class FootballTeamApplication :  Application<FootballTeam>, IFootballTeamApplication
    {
        IFootballTeamRepository _repo;
        public FootballTeamApplication(IFootballTeamRepository repo) : base(repo)
        {
            _repo = repo;
        }

        public FootballTeam GetFootballTeamByManager(string manager)
        {
            return _repo.GetFootballTeamByManager(manager);
        }
    }
}
